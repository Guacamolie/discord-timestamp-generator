app-title = Zeitspempel&shy;generator für Discord
timezone-select-prefix = Ich habe das Datum und die Uhrzeit in
timezone-select-local = meiner eigenen Zeitzone ({ $local-timezone-name })
    gegeben
timezone-select-utc = UTC gegeben
snippet-description = Kopiere diesen Ausschnitt und füge ihn in deine Nachricht
    ein. Dein Ausgewähltes Datum und die Uhrzeit werden anderen in deren eigener
    Sprache und Zeitzone angezeigt.
snippet-description-invalid = Das Datum / Die Uhrzeit die du eingegeben hast
    scheint ungültig zu sein.
snippet-button-copy = Kopieren
language-switcher-prefix = Auch verfügbar in
# language-switcher-suffix =
