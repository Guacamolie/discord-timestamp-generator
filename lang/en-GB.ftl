app-title = Timestamp Generator for Discord
timezone-select-prefix = I entered the date and time in
timezone-select-local = My local timezone ({ $local-timezone-name })
timezone-select-utc = UTC
snippet-description = Copy this snippet and paste it into your message.
    Everyone will see the date and time you selected in their own timezone and
    language.
snippet-description-invalid = The date and/or time you entered doesn't appear
    to be valid.
snippet-button-copy = Copy
language-switcher-prefix = Also available in
# language-switcher-suffix =
