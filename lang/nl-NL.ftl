app-title = Tijdstempel&shy;generator voor Discord
timezone-select-prefix = Ik heb de datum en tijd ingevoerd in
timezone-select-local = Mijn lokale tijdzone ({ $local-timezone-name })
timezone-select-utc = UTC
snippet-description = Kopieer het volgende fragment en plak het in je bericht.
    Iedereen zal de door jouw aangegeven datum en tijd in hun eigen tijdzone en
    taal zien.
snippet-description-invalid = De datum en/of tijd die je hebt ingevoerd lijkt
    niet geldig te zijn.
snippet-button-copy = Kopiëren
language-switcher-prefix = Ook beschikbaar in
# language-switcher-suffix =
