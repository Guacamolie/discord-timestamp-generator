app-title = Генератор отметки времени для Discord
timezone-select-prefix = Я ввёл дату и время в
timezone-select-local = Своей часовой зоне ({ $local-timezone-name })
timezone-select-utc = UTC
snippet-description = Скопируй этот фрагмент и вставь его в своё сообщение.
    Все участники увидят ту же дату и время, но адаптированную под их часовой
    пояс и язык.
snippet-description-invalid = Некорректный формат даты и/или времени
snippet-button-copy = Копировать
language-switcher-prefix = Также доступно на
# language-switcher-suffix =
