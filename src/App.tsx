import { Localized } from "@fluent/react";
import { DateTime } from "luxon";
import { useState } from "react";
import DateTimeInput from "./DateTimeInput";
import LanguageSwitcher from "./LanguageSwitcher";
import Snippet from "./Snippet";

export default function App() {
	const [datetime, setDatetime] = useState(DateTime.now().set({ second: 0, millisecond: 0 }))

	console.log(datetime.toISO());

	return <div className="App">
		<Localized id="app-title"><h1 /></Localized>
		<LanguageSwitcher />
		<DateTimeInput initailDatetime={datetime} onDateTimeChange={setDatetime} />
		{datetime.isValid
			? <Snippet timestamp={datetime.toSeconds()} />
			: <Snippet timestamp={null} />
		}
	</div>;
}
