import { createContext, ReactChild, useMemo, useState } from 'react';
import { negotiateLanguages } from '@fluent/langneg';
import { FluentBundle, FluentResource } from '@fluent/bundle';
import { LocalizationProvider, ReactLocalization } from '@fluent/react';

import germanFluentSource from "../lang/de-DE.ftl"
import englishFluentSource from "../lang/en-GB.ftl"
import dutchFluentSource from "../lang/nl-NL.ftl"
import russianFluentSource from "../lang/ru-RU.ftl"

export type Language = "de-DE" | "en-GB" | "nl-NL" | "ru-RU";
export const supportedLanguages: Record<Language, string> = {
	"de-DE": "Deutsch",
	"en-GB": "English",
	"nl-NL": "Nederlands",
	"ru-RU": "Русский",
};

export type LanguageContextValue = {
	language: Language,
	setLanguage: (language: Language) => void,
}

export const LanguageContext = createContext<LanguageContextValue>({
	language: 'en-GB',
	setLanguage: (language) => {},
})

export type AppLocalizationProviderProps = {
	children: ReactChild,
}

export default function AppLocalizationProvider({ children }: AppLocalizationProviderProps) {
	const [chosenLanguage, setChosenLanguage] = useState<'default' | Language>('default');

	// Store all translations as a simple object which is available 
	// synchronously and bundled with the rest of the code.
	const resources: { [K in Language]: FluentResource } = useMemo(() => ({
		'de-DE': new FluentResource(germanFluentSource),
		'en-GB': new FluentResource(englishFluentSource),
		'nl-NL': new FluentResource(dutchFluentSource),
		'ru-RU': new FluentResource(russianFluentSource),
	}), [englishFluentSource, dutchFluentSource]);

	// Auto-detect a list of support languages that the user prefers.
	const detectedLanguagePrefList = negotiateLanguages(
		navigator.languages,
		Object.keys(resources),
		{ defaultLocale: 'en-GB' }
	) as Language[];

	const languages = chosenLanguage == 'default'
		? detectedLanguagePrefList
		: [chosenLanguage, ...detectedLanguagePrefList];

	// A generator function responsible for building the sequence 
	// of FluentBundle instances in the order of user's language
	// preferences.
	function* generateBundles(locals: readonly Language[]) {
		for (const locale of locals) {
			const bundle = new FluentBundle(locale);
			bundle.addResource(resources[locale]);
			yield bundle;
		}
	}

	// The ReactLocalization instance stores and caches the sequence of generated
	// bundles. You can store it in your app's state.
	const l10n = new ReactLocalization(generateBundles(languages));

	const languageContextValue: LanguageContextValue = {
		language: languages[0],
		setLanguage: (language) => {
			if (language == detectedLanguagePrefList[0]) {
				// The chosen language is the same as the language the browser
				// communicated to us. Return to defaults.
				setChosenLanguage('default');
			} else {
				setChosenLanguage(language);
			}
		},
	};

	return <LocalizationProvider l10n={l10n}>
		<LanguageContext.Provider value={languageContextValue}>
			{children}
		</LanguageContext.Provider>
	</LocalizationProvider>
}
