import { Localized } from "@fluent/react";

export type SnippetProps = {
	timestamp: number | null,
}

export default function Snippet({ timestamp }: SnippetProps) {
	const snippet = `<t:${timestamp}>`;

	function onCopyButtonClick() {
		navigator.clipboard.writeText(snippet);
	}

	if (timestamp === null) {
		return <div className="Snippet card">
			<Localized id="snippet-description-invalid"><p /></Localized>
			<p>
				<code>
					<span className="snippet-text">(╯°□°）╯︵ ┻━┻</span>
				</code>
			</p>
		</div>
	}

	return <div className="Snippet card">
		<Localized id="snippet-description"><p /></Localized>
		<p>
			<code>
				<span className="snippet-text">{snippet}</span>
				<Localized id="snippet-button-copy">
					<button onClick={onCopyButtonClick}></button>
				</Localized>
			</code>
		</p>
	</div>
}
