import { ChangeEvent, useEffect, useState } from "react";
import { DateTime, FixedOffsetZone, SystemZone, Zone } from "luxon";
import { uniqID } from "./utils";
import { Localized } from "@fluent/react";

type TimezoneSelectProps = {
	timezone: Zone,
	setTimezone: (zone: Zone) => void,
}

function TimezoneSelect({ timezone, setTimezone }: TimezoneSelectProps) {
	const [radioInputName, _] = useState(() => `timezone-select-${uniqID()}`)
	const localZone = SystemZone.instance;
	const utcZone = FixedOffsetZone.utcInstance;

	function onSelect(event: ChangeEvent<HTMLInputElement>) {
		if (event.target.value !== "local" && event.target.value !== "utc") {
			throw new Error("invalid target value");
		}

		switch (event.target.value) {
			case "local":
				setTimezone(localZone);
				break;
			case "utc":
				setTimezone(utcZone);
				break;
		}
	}

	return <fieldset className="TimezoneSelect card">
		<legend><Localized id="timezone-select-prefix"><p /></Localized></legend>
		<p>
			<label>
				<input
					type="radio"
					value="local"
					name={radioInputName}
					checked={timezone.equals(localZone)}
					onChange={onSelect}
				/>
				<Localized id="timezone-select-local" vars={{ 'local-timezone-name': localZone.name }} />
			</label>
			<br />
			<label>
				<input
					type="radio"
					value="utc"
					name={radioInputName}
					checked={timezone.equals(utcZone)}
					onChange={onSelect}
				/>
				<Localized id="timezone-select-utc" />
			</label>
		</p>
	</fieldset>;
}

export type DateTimeInputProps = {
	initailDatetime: DateTime,
	onDateTimeChange: (datetime: DateTime) => void,
}

export default function DateTimeInput({ initailDatetime , onDateTimeChange }: DateTimeInputProps) {
	const [timezone, setTimezone] = useState(() => initailDatetime.zone);
	const [dateInput, setDateInput] = useState(() => initailDatetime.toISODate());
	const [timeInput, setTimeInput] = useState(() => initailDatetime.toISOTime({ suppressSeconds: true, includeOffset: false }));

	useEffect(() => {
		let newDateTime = DateTime.fromISO(dateInput + "T" + timeInput, {
			zone: timezone,
		});

		onDateTimeChange(newDateTime);
	}, [timezone, dateInput, timeInput, onDateTimeChange]);

	return <div className="DateTimeInput">
		<div className="input-wrapper">
			<input
				type="date"
				value={dateInput}
				onChange={event => setDateInput(event.target.value)}
				/>
			<input
				type="time"
				value={timeInput}
				onChange={event => setTimeInput(event.target.value)}
				/>
		</div>
		<TimezoneSelect timezone={timezone} setTimezone={setTimezone} />
	</div>;
}
