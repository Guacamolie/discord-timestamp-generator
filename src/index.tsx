import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import AppLocalizationProvider from "./AppLocalizationProvider";

ReactDOM.render(
	<React.StrictMode>
		<AppLocalizationProvider>
			<App />
		</AppLocalizationProvider>
	</React.StrictMode>,
	document.querySelector("#root")
);
