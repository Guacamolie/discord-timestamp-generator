import { Localized } from "@fluent/react";
import { useContext } from "react"
import { Language, LanguageContext, supportedLanguages } from "./AppLocalizationProvider"

export default function LanguageSwitcher() {
	const language = useContext(LanguageContext);

	return <p className="LanguageSwitcher">
		<Localized id="language-switcher-prefix" />
		{Object.entries(supportedLanguages)
			.filter(([langCode]) => langCode != language.language)
			.map(([langCode, langName]) =>
				<button
					key={langCode}
					onClick={() => language.setLanguage(langCode as Language)}>
						{langName}
				</button>
			)
		}
		<Localized id="language-switcher-suffix" />
	</p>;
}
